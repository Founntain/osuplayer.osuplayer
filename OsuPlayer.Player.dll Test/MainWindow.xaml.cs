﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;

/*

    OsuPlayer.Player.dll Test writen by Founntain (c) Founntain 2019
    Code can be modified as any whish. At least give credit

    If you have problems running this project, please watch into the lib folder and copy all DLLs to the Debug/Release folder. Because you need these to run the project.
 */

namespace OsuPlayer.Player.Test
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        //Variables
        List<string> Paths = new List<string>();
        private Player Player;
        DispatcherTimer Timer = new DispatcherTimer();

        public MainWindow()
        {
            InitializeComponent();

            try
            {
                MessageBox.Show("Please select some mp3 files or one");

                //File dialog for mp3 file selecting
                var od = new CommonOpenFileDialog {Multiselect = true};

                if(od.ShowDialog() != CommonFileDialogResult.Ok) Close();

                Paths = od.FileNames.ToList();

                //Registering bass.net
                Player.RegisterBassNet("", "");

                //Creating Player object
                Player = new Player(Paths);

                Player.Names = Player.GenerateSongnameListFromPathList();

                Console.WriteLine();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                Close();
            }

            //Init timer
            Timer.Interval = TimeSpan.FromMilliseconds(100);
            Timer.Tick += Timer_Tick;
        }

        //Timer for refreshing title, path, songtime etc
        private void Timer_Tick(object source, EventArgs e)
        {
            try
            {
                if (Player.Playerstate != Player.Playstate.Playing) return;

                if (SliderChanged == false) progressSlider.Value = Player.CurrentTime;
                
                TimeTxt.Text = $"{Player?.Format((int)Player?.CurrentTime)} : {Player?.Format((int)Player?.MaxTime)}";

                if(Player?.CurrentTime >= Player?.MaxTime - .5) Player.Next((float)slider?.Value / 100);

                if (!Player.NewSong) return;

                song.Text = Player?.SongTitle;
                path.Text = Player?.CurrentIndex >= 0 ? Player?.Paths[Player.CurrentIndex] : "Path not in list, did you used Player.PlayFromPath()?";
                progressSlider.Maximum = Player.MaxTime;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        //Play, pause button
        private void PlayPauseBtn_Click(object sender, RoutedEventArgs e)
        {
            if(Player.Playerstate == Player.Playstate.Stopped)
            {
                Player.Play(0, (float)slider?.Value / 100);
                Player.SetPlaypackSpeed(44100);
            }
            else
                Player.PlayPause();

            if (Player.Playerstate == Player.Playstate.Playing) Timer.Start();
            else Timer.Stop();
        }


        //next button
        private void NextBtn_Click(object sender, RoutedEventArgs e)
        {
            Player.Next((float)slider.Value / 100);
        }

        //Volume slider
        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (slider == null || volTxt == null) return;

            Player?.SetVolume((float)slider.Value / 100);
            volTxt.Text = $"Volume: {Math.Round(slider.Value, 2)}";
        }

        //prev button
        private void PrevBtn_OnClick(object sender, RoutedEventArgs e)
        {
            Player.Prev((float)slider.Value / 100);
        }

        //playback slider
        private void SliderPlayback_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (sliderPlayback == null || Playback == null) return;

            Playback.Text = $"Playback: {(int) sliderPlayback.Value}";
            if (!(bool) Player?.SetPlaypackSpeed((float) sliderPlayback.Value))
            {
                MessageBox.Show("Can't use playback feature because bass_fx.dll was not found");
            }
        }

        //Custom path button
        private void PlayPath_Click(object sender, RoutedEventArgs e)
        {
            var od = new OpenFileDialog();

            try
            {
                if (od.ShowDialog() == true) Player.PlayFromPtath(od.FileName, (float) slider.Value / 100);

                if (Player.Playerstate == Player.Playstate.Playing) Timer.Start();
                else Timer.Stop();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Invalid file\n\n" + exception);
            }
        }

        //Variable and three methods for the progress slider
        private bool SliderChanged;

        private void ProgressSlider_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (Player.Playerstate == Player.Playstate.Playing)
                Player.PlayPause();
            SliderChanged = true;
        }

        private void ProgressSlider_PreviewMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Player.PlayPause();
            SliderChanged = false;
        }

        private void ProgressSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (SliderChanged) Player.Engine.ChannelPosition = progressSlider.Value;
        }
    }
}
