# OsuPlayer.OsuPlayer

A class library written in C# for the osu!player plus. It contains all functions that the 
music player in the osu!player has. There is also a demo app in this repository, that contains
creating an object, Play songs, next/prev, Volume, Playbackspeed (Daycore / Nightcore).

#### Note

You can modify or edit the code to your own.