﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Timers;
using Un4seen.Bass;
using Un4seen.Bass.AddOn.Fx;

namespace OsuPlayer.Player
{
    public class BassEngine
    {
        private const int REPEAT_THRESHOLD = 200;
        private static BassEngine Engine;
        private readonly SYNCPROC EndTrackSyncProc;
        private readonly Timer PositionTimer = new Timer();
        private readonly SYNCPROC RepeatSyncProc;
        private int ActiveStream;
        private double ChannelLengthD;
        private double CurrentChannelPosition;
        private bool InChannelSet;
        private bool InChannelTimerUpdate;
        private bool InRepeatSet;
        private TimeSpan RepeatStart;
        private TimeSpan RepeatStop;
        private int RepeatSyncId;
        public int SampleFrequency = 44100;
        private int StreamFx;

        private BassEngine()
        {
            Initialize();
            EndTrackSyncProc = EndTrack;
            RepeatSyncProc = RepeatCallback;
        }

        public double Difference { get; set; }

        #region Singleton Instance

        public static BassEngine Instance => Engine ?? (Engine = new BassEngine());

        #endregion

        public int ActiveStreamHandle
        {
            get => ActiveStream;
            protected set
            {
                var oldValue = ActiveStream;
                ActiveStream = value;
                if (oldValue != ActiveStream)
                    NotifyPropertyChanged("ActiveStreamHandle");
            }
        }

        public int FxStream
        {
            get => StreamFx;
            protected set
            {
                var oldValue = StreamFx;
                StreamFx = value;
                if (oldValue != StreamFx)
                    NotifyPropertyChanged("FXStream");
            }
        }

        private void PositionTimer_Tick(object sender, EventArgs e)
        {
            if (FxStream == 0)
            {
                ChannelPosition = 0;
            }
            else
            {
                InChannelTimerUpdate = true;
                ChannelPosition = Bass.BASS_ChannelBytes2Seconds(FxStream, Bass.BASS_ChannelGetPosition(FxStream, 0));
                InChannelTimerUpdate = false;
            }
        }


        #region Player

        public TimeSpan SelectionBegin
        {
            get => RepeatStart;
            set
            {
                if (!InRepeatSet)
                {
                    InRepeatSet = true;
                    var oldValue = RepeatStart;
                    RepeatStart = value;
                    if (oldValue != RepeatStart)
                        NotifyPropertyChanged("SelectionBegin");
                    SetRepeatRange(value, SelectionEnd);
                    InRepeatSet = false;
                }
            }
        }

        public TimeSpan SelectionEnd
        {
            get => RepeatStop;
            set
            {
                if (!InChannelSet)
                {
                    InRepeatSet = true;
                    var oldValue = RepeatStop;
                    RepeatStop = value;
                    if (oldValue != RepeatStop)
                        NotifyPropertyChanged("SelectionEnd");
                    SetRepeatRange(SelectionBegin, value);
                    InRepeatSet = false;
                }
            }
        }

        public double ChannelLength
        {
            get => ChannelLengthD;
            protected set
            {
                var oldValue = ChannelLengthD;
                ChannelLengthD = value;
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                if (oldValue != ChannelLengthD)
                    NotifyPropertyChanged("ChannelLength");
            }
        }

        public double ChannelPosition
        {
            get => CurrentChannelPosition;
            set
            {
                if (InChannelSet) return;

                InChannelSet = true; // Avoid recursion
                var oldValue = CurrentChannelPosition;
                var position = Math.Max(0, Math.Min(value, ChannelLength));
                if (!InChannelTimerUpdate)
                    Bass.BASS_ChannelSetPosition(FxStream, Bass.BASS_ChannelSeconds2Bytes(FxStream, position));
                CurrentChannelPosition = position;
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                if (oldValue != CurrentChannelPosition)
                    NotifyPropertyChanged("ChannelPosition");
                InChannelSet = false;
            }
        }

        #endregion

        #region Callbacks

        private void EndTrack(int handle, int channel, int data, IntPtr user)
        {
            Stop();
        }

        private void RepeatCallback(int handle, int channel, int data, IntPtr user) => ChannelPosition = SelectionBegin.TotalSeconds;

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }

        #endregion

        #region Public Methods

        public void Stop()
        {
            ChannelPosition = SelectionBegin.TotalSeconds;
            if (FxStream != 0)
            {
                Bass.BASS_ChannelStop(FxStream);
                Bass.BASS_ChannelSetPosition(FxStream, ChannelPosition);
            }
        }

        public void Pause()
        {
            Bass.BASS_ChannelPause(FxStream);
        }

        public void Play()
        {
            PlayCurrentStream();
        }

        public void SetVolume(float volume)
        {
            try
            {
                //if(FXStream != 0)
                Bass.BASS_ChannelSetAttribute(FxStream, BASSAttribute.BASS_ATTRIB_VOL, volume);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        public bool OpenFile(string path)
        {
            Stop();

            if (FxStream != 0)
            {
                ClearRepeatRange();
                ChannelPosition = 0;
                Bass.BASS_StreamFree(FxStream);
            }

            if (File.Exists(path))
            {
                // Create Stream
                ActiveStreamHandle = Bass.BASS_StreamCreateFile(path, 0, 0,
                    BASSFlag.BASS_STREAM_DECODE | BASSFlag.BASS_SAMPLE_FLOAT | BASSFlag.BASS_STREAM_PRESCAN);
                FxStream = BassFx.BASS_FX_TempoCreate(ActiveStreamHandle,
                    BASSFlag.BASS_FX_FREESOURCE | BASSFlag.BASS_SAMPLE_FLOAT | BASSFlag.BASS_SAMPLE_LOOP);
                ChannelLength = Bass.BASS_ChannelBytes2Seconds(FxStream, Bass.BASS_ChannelGetLength(FxStream, 0));
                if (FxStream != 0)
                {
                    // Obtain the sample rate of the stream
                    var info = new BASS_CHANNELINFO();
                    Bass.BASS_ChannelGetInfo(FxStream, info);
                    SampleFrequency = info.freq;
                    Difference = 44100 - SampleFrequency;


                    // Set the stream to call Stop() when it ends.
                    var syncHandle = Bass.BASS_ChannelSetSync(FxStream,
                        BASSSync.BASS_SYNC_END,
                        0,
                        EndTrackSyncProc,
                        IntPtr.Zero);

                    if (syncHandle == 0)
                        throw new ArgumentException(@"Error establishing End Sync on file stream.", nameof(path));

                    return true;
                }

                ActiveStreamHandle = 0;
                FxStream = 0;
            }

            return false;
        }

        #endregion

        #region Init

        private void Initialize()
        {
            PositionTimer.Interval = TimeSpan.FromMilliseconds(50).TotalMilliseconds;
            PositionTimer.Elapsed += PositionTimer_Tick;
            PositionTimer.Start();


            if (!Bass.BASS_Init(-1, 44100, BASSInit.BASS_DEVICE_SPEAKERS, IntPtr.Zero))
                Console.WriteLine("Bass initialization error!");
        }

        private void SetRepeatRange(TimeSpan startTime, TimeSpan endTime)
        {
            if (RepeatSyncId != 0)
                Bass.BASS_ChannelRemoveSync(FxStream, RepeatSyncId);

            if (endTime - startTime > TimeSpan.FromMilliseconds(REPEAT_THRESHOLD))
            {
                var channelLength = Bass.BASS_ChannelGetLength(FxStream);
                var endPosition = (long) (endTime.TotalSeconds / ChannelLength * channelLength);
                RepeatSyncId = Bass.BASS_ChannelSetSync(FxStream,
                    BASSSync.BASS_SYNC_POS,
                    endPosition,
                    RepeatSyncProc,
                    IntPtr.Zero);
                ChannelPosition = SelectionBegin.TotalSeconds;
            }
            else
            {
                ClearRepeatRange();
            }
        }

        private void ClearRepeatRange()
        {
            if (RepeatSyncId == 0) return;

            Bass.BASS_ChannelRemoveSync(FxStream, RepeatSyncId);
            RepeatSyncId = 0;
        }

        private void PlayCurrentStream()
        {
            // Play Stream
            if (FxStream != 0 && Bass.BASS_ChannelPlay(FxStream, false))
            {
                // Do nothing
            }
        }

        #endregion
    }
}