﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using OsuPlayer.Player.Exceptions;
using Un4seen.Bass;

/*

    OsuPlayer.Player writen by Founntain (c) Founntain 2019
    Code can be modified as any whish. At least give credit

 */

namespace OsuPlayer.Player
{
    /// <summary>
    /// Musicplayer class for the osu!player plus
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Is the player playing, paused or stopped?
        /// Playing => The player is currently playing the song
        /// Paused => The player is currently paused the song
        /// Stopped => The player is currently in stopped mode (Playing nothing and no song loaded yet). Mostly used, when the player was initialized.
        /// </summary>
        public enum Playstate
        {
            Playing,
            Paused,
            Stopped
        }

        /// <summary>
        /// Soundengine for the Player.
        /// </summary>
        public static BassEngine Engine;

        /// <summary>
        /// Is the current song a new song? If not probably song repeated
        /// </summary>
        public bool NewSong;

        /// <summary>
        /// The History for shuffle, contains only last 10 songs
        /// </summary>
        public int[] ShuffleHistory = new int[13];
        public int ShuffleHistoryCount;

        /// <summary>
        /// The current zero based index of the playing song, if the song is played from <see cref="Player.PlayFromPtath(string, float, string)" /> then it will be -1
        /// </summary>
        public int CurrentIndex { get; set; }

        /// <summary>
        /// List of all song names
        /// </summary>
        public List<string> Names { get; set; }

        /// <summary>
        /// List of all song paths
        /// </summary>
        public List<string> Paths { get; set; }

        /// <summary>
        /// List of all song backgrounds
        /// </summary>
        public List<string> Backgrounds { get; set; }

        /// <summary>
        /// List of all song names (not their paths), from the current selected playlist.
        /// </summary>
        public List<string> CustomPlaylist { get; set; }

        /// <summary>
        /// <see cref="Player.Playstate" /> for the player class
        /// </summary>
        public Playstate Playerstate { get; set; }

        /// <summary>
        /// Name of the current playing song
        /// </summary>
        public string SongTitle { get; set; }

        /// <summary>
        /// Max song duration
        /// </summary>
        public double MaxTime => Engine.ChannelLength;

        /// <summary>
        /// Current song time
        /// </summary>
        public double CurrentTime => Engine.ChannelPosition;

        /// <summary>
        /// Is the player in shuffle mode?
        /// </summary>
        public bool Shuffle { get; set; }

        /// <summary>
        /// Is the player in repeat mode?
        /// </summary>
        public bool Repeat { get; set; }

        /// <summary>
        /// Playback speed for the Nightcore/Daycore feature
        /// </summary>
        public float Playback { get; set; }

        /// <summary>
        /// Player Instance without anything
        /// </summary>
        public Player()
        {
            Engine = BassEngine.Instance;
            Playerstate = Playstate.Stopped;
        }

        /// <summary>
        /// Creates a new Instance of OsuPlayerPlus.Player
        /// </summary>
        /// <param name="paths">List of all absolute pathes, from your songs</param>
        /// <param name="names">List of all songnames, if the list doesn't exist, leave it and names will be the same as paths</param>
        /// <param name="backgrounds">List of Backgrounds, if needed</param>
        public Player(List<string> paths, List<string> names = null, List<string> backgrounds = null)
        {

            if(paths == null) throw new PathListNullException();

            Names = names ?? paths;
            Paths = paths;
            Backgrounds = backgrounds;
            Engine = BassEngine.Instance;
            CurrentIndex = 0;

            Playerstate = Playstate.Stopped;
        }

        /// <summary>
        /// Register the Bass.Net with your information
        /// </summary>
        /// <param name="bassNetEmail">Your bass.net email</param>
        ///  <param name="bassNetToken">Your bass.net token</param>
        public static void RegisterBassNet(string bassNetEmail, string bassNetToken) => BassNet.Registration(bassNetEmail, bassNetToken);
        

        /// <summary>
        /// Sets the volume of the player
        /// </summary>
        /// <param name="volume">New Volume</param>
        public void SetVolume(float volume)
        {
            Engine.SetVolume(volume);
        }

        /// <summary>
        /// Play a song by the given zero based Index
        /// </summary>
        /// <param name="songindex">Zero based song index</param>
        /// <param name="volume">Current / New Volume</param>
        public void Play(int songindex, float volume = 10)
        {
            if (songindex == -1)
            {
                Play(Names.Count - 1, volume);
                return;
            }

            NewSong = true;

            try
            {
                Engine.OpenFile(Paths[songindex]);
                Engine.Play();

                Engine.SetVolume(volume);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }

            SetPlaypackSpeed(Playback);

            SongTitle = Names[songindex];

            CurrentIndex = songindex;

            Playerstate = Playstate.Playing;
        }

        /// <summary>
        /// Same as the normal Play method, except that you can force if it is a new song. E.g to reload song title or stuff like that
        /// </summary>
        /// <param name="songindex"></param>
        /// <param name="volume"></param>
        /// <param name="forceNewSong"></param>
        public void Play(int songindex, float volume, bool forceNewSong)
        {
            NewSong = forceNewSong;

            try
            {
                Engine.OpenFile(Paths[songindex]);
                Engine.Play();

                Engine.SetVolume(volume);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }

            SetPlaypackSpeed(Playback);

            SongTitle = Names[songindex];

            CurrentIndex = songindex;

            Playerstate = Playstate.Playing;
        }

        /// <summary>
        /// Play a file from the absolute path
        /// </summary>
        /// <param name="path">The absolute file path</param>
        /// <param name="volume">Current/New volume</param>
        /// <param name="songname">Songname of the file, if not given the name will be the path</param>
        public void PlayFromPtath(string path, float volume, string songname = null)
        {
            try
            {
                Engine.OpenFile(path);
                Engine.Play();

                Engine.SetVolume(volume);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }

            SetPlaypackSpeed(Playback);

            SongTitle = songname ?? path;

            CurrentIndex = -1;

            Playerstate = Playstate.Playing;
        }

        /// <summary>
        /// Force the song to Resume
        /// </summary>
        public void ForcePlay()
        {
            Engine.Play();
        }

        /// <summary>
        /// Resume or Pause the current song
        /// </summary>
        public void PlayPause()
        {
            if (Playerstate == Playstate.Paused)
            {
                Engine.Play();
                Playerstate = Playstate.Playing;
            }
            else
            {
                Engine.Pause();
                Playerstate = Playstate.Paused;
            }
        }

        /// <summary>
        /// Plays the next song
        /// </summary>
        /// <param name="volume">Current/new volume</param>
        public void Next(float volume)
        {
            if (Repeat)
            {
                Play(CurrentIndex, volume);
                return;
            }

            if (Shuffle)
            {
                Play(DoShuffle("next"), volume);
                return;
            }

            if (CurrentIndex == Names.Count - 1)
                Play(0, volume);
            else
                Play(CurrentIndex + 1, volume);
        }

        /// <summary>
        /// Plays a random song
        /// </summary>
        /// <param name="direction">Can be "prev" or "next" depending if you want to go back or forth</param>
        /// <returns></returns>
        private int DoShuffle(string direction)
        {
            int shuffleIndex;
            var rdm = new Random();
            if (direction == "next")
            {
                ShuffleHistory[ShuffleHistoryCount] = CurrentIndex;


                if (ShuffleHistory[ShuffleHistoryCount + 1] == 0 && ShuffleHistory[ShuffleHistoryCount + 2] == 0)
                {
                    if (ShuffleHistoryCount < 9)
                        ShuffleHistoryCount++;
                    else
                        Array.Copy(ShuffleHistory, 1, ShuffleHistory, 0, ShuffleHistory.Length - 1);

                    shuffleIndex = rdm.Next(0, Names.Count);

                    while (shuffleIndex == CurrentIndex) shuffleIndex = rdm.Next(0, Names.Count);
                }
                else
                {
                    ShuffleHistoryCount++;
                    shuffleIndex = ShuffleHistory[ShuffleHistoryCount];
                }

                Debug.WriteLine("ShuffleHistory: " + ShuffleHistoryCount);
                ShuffleHistory[ShuffleHistoryCount] = shuffleIndex;

                return shuffleIndex;
            }

            if (ShuffleHistoryCount > 0)
            {
                ShuffleHistoryCount--;
                shuffleIndex = ShuffleHistory[ShuffleHistoryCount];
            }
            else
            {
                ShuffleHistory[ShuffleHistoryCount] = CurrentIndex;
                Array.Copy(ShuffleHistory, 0, ShuffleHistory, 1, ShuffleHistory.Length - 1);
                shuffleIndex = rdm.Next(0, Names.Count);
                ShuffleHistory[ShuffleHistoryCount] = shuffleIndex;
            }

            Debug.WriteLine("ShuffleHistory: " + ShuffleHistoryCount);

            return shuffleIndex;
        }

        /// <summary>
        /// Checks if the current song is in the current selected playlist
        /// </summary>
        /// <param name="songTitle"></param>
        /// <returns></returns>
        public bool CheckIfSongIsInCurrentPlaylist(string songTitle)
        {
            return CustomPlaylist != null && CustomPlaylist.Contains(songTitle);
        }

        /// <summary>
        /// Go back one song
        /// </summary>
        /// <param name="volume">Current/New volume</param>
        public void Prev(float volume)
        {
            if (Engine.ChannelPosition > 3)
            {
                Play(CurrentIndex, volume);
                return;
            }

            if (Shuffle)
            {
                Play(DoShuffle("prev"), volume);
                return;
            }

            if (CurrentIndex != 0)
                Play(CurrentIndex - 1, volume);
            else
                Play(Names.Count - 1, volume);
        }

        /// <summary>
        /// Generates a <see cref="Names"/>-List from the <see cref="Paths"/>-List. If the list was generated succesfully it will be saved in the <see cref="Names"/>-List
        /// </summary>
        /// <returns>Returns the generated List.</returns>
        public List<string> GenerateSongnameListFromPathList(){

            if(Paths == null) throw new PathListNullException();

            var results = new List<string>();

            foreach(var path in Paths){

                var splittedPath = path.Split('\\');

                results.Add(splittedPath.Last().Substring(0, splittedPath.Last().Length - 4) ?? "Couldn't resolve songname");
            }

            Names = results;

            return results;
        }

        /// <summary>
        /// Searches your songs and returns a list with all results
        /// </summary>
        /// <param name="keywords">Keywords for the search</param>
        /// <returns></returns>
        public List<string> SearchSongs(string keywords)
        {
            var result = new List<string>();

            if (Names == null) return result;

            try
            {
                if (Names.Count <= 0) return result;

                return string.IsNullOrEmpty(keywords) || string.IsNullOrWhiteSpace(keywords)
                    ? result
                    : Names.Where(x => x.ToLower().Contains(keywords.ToLower())).ToList();
            }
            catch (Exception)
            {
                return result;
            }
        }

        /// <summary>
        /// Gets the zero based index from a song
        /// </summary>
        /// <param name="song">Songname</param>
        /// <returns></returns>
        public int GetSongIndex(string song)
        {
            return Names?.FindIndex(x => x.Contains(song)) ?? -1;
        }

        /// <summary>
        /// Formates the time to a hh:mm:ss format
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public string Format(int seconds)
        {
            var timeDif = TimeSpan.FromSeconds(seconds);
            var time = timeDif.ToString(@"hh\:mm\:ss");
            return time;
        }

        /// <summary>
        /// Nightcore / Daycore method, depending on the speed. Needs bass_fx.dll for this feature.
        /// The default value is 44100
        /// </summary>
        /// <param name="speed"></param>
        public bool SetPlaypackSpeed(float speed)
        {
            var successfull = TrySetPlaybackSpeed(speed, out var playback);

            Playback = successfull ? playback : Playback;

            return successfull;
        }

        /// <summary>
        /// Check if the value is valid for the <see cref="BassEngine"/> if so the it will return true and outs the new value.
        /// If not it will return the default value
        /// </summary>
        /// <param name="speed"></param>
        /// <param name="playback"></param>
        /// <returns></returns>
        public bool TrySetPlaybackSpeed(float speed, out float playback)
        {
            var result = Bass.BASS_ChannelSetAttribute(BassEngine.Instance.FxStream, BASSAttribute.BASS_ATTRIB_TEMPO_FREQ,
                    speed - (float) BassEngine.Instance.Difference);

            playback = result ? speed : 44100;

            return result;
        }
    }
}
