using System;

namespace OsuPlayer.Player.Exceptions {
    public class PathListNullException : Exception{
        public PathListNullException() : base("The paths list can not be null"){
            
        }

    }
}